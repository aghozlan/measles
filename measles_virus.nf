#!/usr/bin/env nextflow

workflow.onComplete = {
    // any workflow property can be used here
    println "Pipeline complete"
    println "Command line: $workflow.commandLine"
}


workflow.onError = {
    println "Oops .. something went wrong"
}

params.help=false

def usage() {
    println("measle_virus.nf --in <reads_dir> --out <output_dir> --cpus <nb_cpus> -w <temp_work_dir>")
}


if(params.help){
    usage()
    exit(1)
}

params.in="$baseDir/test/"
params.alienseq = "$baseDir/databases/alienTrimmerPF8contaminants.fasta"
params.human_genome_bowtie = "/local/databases/index/bowtie/2.3.5.1/hg19.fa"
params.human_genome_picard = "/local/databases/index/picard/hg19/2.23.3/hg19.fa"
params.human_genome_gatk ="/local/databases/index/gatk/hg19/2.4-9/hg19.fa"
params.measles_genome = "$baseDir/databases/Measles_morbillivirus.fna"
params.mitochondrion_genome = "$baseDir/databases/chrM.fna"
params.out = "$baseDir/annotation/"
params.cpus = 4
params.minimum_read_len = 35
myDir = file(params.out)
myDir.mkdirs()

readChannel = Channel.fromPath("${params.in}/*.{fastq,fq,fq.gz,fastq.gz}")
                    .map {file -> tuple(file.baseName.replaceAll(".gz","").replaceAll(".fastq",""),file)}
                    .ifEmpty { exit 1, "Cannot find any reads matching: ${params.in}" }
                    //.subscribe { println it }

process trimming {
    //publishDir "$myDir", mode: 'copy'
    memory "1G"

    input:
    set read_id, file(reads) from readChannel

    output:
    set read_id, file("*_trim.fastq") into cleanChannel
    set read_id, file("*_trim.fastq") into cleanChannelBis

    script:
    """
    AlienTrimmer -i ${reads} -q 20  -p 80 -k 10 -m 5 -l ${params.minimum_read_len}  -o ${read_id}_trim -a ${params.alienseq}
    """
}

process human {
    //publishDir "$myDir", mode: 'copy'
    cpus params.cpus
    memory "5G"

    input:
    set read_id, file(reads) from cleanChannel

    output:
    set read_id, file("*.sam") into samhumanChannel
    set read_id, file("*.fastq") into nohumanChannel

    script:
    """
    bowtie2 --rg-id ${read_id} --rg SM:${read_id} -q -U ${reads} -x ${params.human_genome_bowtie} -S ${read_id}.sam \
            -p ${params.cpus} --un ${read_id}.fastq
    """
}

process measles {
    cpus params.cpus
    memory "5G"
    input:
    set read_id, file(reads) from nohumanChannel

    output:
    set read_id, file("*.sam") into sammeaslesChannel

    script:
    """
    bowtie2 -q -U ${reads} -x ${params.measles_genome} -S ${read_id}.sam \
            -p ${params.cpus}
    """
}

process mitochondrion {
    cpus params.cpus
    memory "5G"
    input:
    set read_id, file(reads) from cleanChannelBis

    output:
    set read_id, file("*.sam") into sammMitoChannel

    script:
    """
    bowtie2 -q -U ${reads} -x ${params.mitochondrion_genome} -S ${read_id}.sam \
            -p ${params.cpus}
    """
}


process counts_human {
    //publishDir "$myDir", mode: 'copy'
    memory "2G"

    input:
    set read_id, file(sam) from samhumanChannel

    output:
    file("*.tsv") into counts_human
    set read_id, file("align_sorted.bam*") into humanbamChannel

    script:
    """
    samtools view ${sam}  -O BAM  -o align.bam
    samtools sort align.bam  -o align_sorted.bam
    samtools index align_sorted.bam
    samtools idxstat align_sorted.bam > ${read_id}_human.tsv
    """
}

process extract_snp {
    publishDir "$myDir", mode: 'copy'
    memory "30G"

    beforeScript "export PICARD_TOOLS_JAVA_OPTS=\"-Xmx20G -Djava.io.tmpdir=\$(pwd)/picardtmp/\""

    input:
    set read_id, file(bam) from humanbamChannel

    output:
    file("*_snp*.tsv") into snpChannel
    file("*_mpileup.vcf.gz") into vcfChannel
    file("*_nodup.bam") into nodupbam

    script:
    """
    picard MarkDuplicates  -I align_sorted.bam  -O ${read_id}_nodup.bam -M marked_dup_metrics.txt --REMOVE_DUPLICATES true 
    picard BuildBamIndex -I ${read_id}_nodup.bam
    bcftools mpileup --skip-indels -d 10000 -f ${params.human_genome_gatk} ${read_id}_nodup.bam -Oz -o  ${read_id}_mpileup.vcf.gz 
    bcftools call --ploidy 1 -cv -Oz -p 1 -o ${read_id}_calls.vcf.gz ${read_id}_mpileup.vcf.gz 
    bcftools filter -Oz -i 'MIN(DP)>5 && QUAL>10 &&CHROM="chrM"' -o ${read_id}_filt.vcf.gz  ${read_id}_calls.vcf.gz  
    bcftools query -f '%CHROM %POS %REF %ALT %DP4\n' ${read_id}_filt.vcf.gz > ${read_id}_snp_bcf.tsv
    """
}
    //-ERC GVCF
    //gatk --java-options "-Xmx20G -Djava.io.tmpdir=\$TMPDIR" HaplotypeCaller -R ${params.human_genome_gatk} -I ${read_id}.bam  -O ${read_id}.vcf
    //gatk VariantsToTable -V ${read_id}.vcf -F CHROM -F POS -F TYPE -GF AD -O ${read_id}_snp.tsv
    //bcftools query -f '%CHROM %POS %REF %ALT{0}\n' ${read_id}.vcf > ${read_id}_snp_bcf.tsv
    // gatk --java-options "-Xmx20G -Djava.io.tmpdir=\$TMPDIR" HaplotypeCaller -R ${params.human_genome_gatk} -I no_duplicates.bam -O output.vcf \
    //     --min-base-quality-score 20 \
    //     --output-mode EMIT_VARIANTS_ONLY \
    //     --emit-ref-confidence BP_RESOLUTION
    // bcftools mpileup -f /local/databases/index/gatk/hg19/2.4-9/hg19.fa no_duplicates.bam | bcftools call -mv -Ob -o calls.bcf

process compute_frequencies {
    publishDir "$myDir", mode: 'copy'

    input:
    file(snp) from snpChannel.toList()

    output:
    file("snp_matrix.tsv") into snp_matrix

    """
    #!/usr/bin/python3
    import os
    import glob
    import csv
    chrM_genome = ""
    seq = False
    with open("${params.mitochondrion_genome}", "rt") as genome:
        for line in genome:
            if line.startswith(">"):
                seq = True
            elif seq and len(line) > 0:
                chrM_genome += line.strip("\\n")
            else:
                print("wtf")
        assert(len(chrM_genome) == 16571)

    count_data = {}
    with open(os.curdir + os.sep + "snp_matrix.tsv", "wt") as output:
        print("Name\\tSNP\\tFrequency", file=output)
        for count_file in glob.glob('{0}/*_snp_bcf.tsv'.format(os.curdir)):
            name = os.path.basename(count_file).replace("_snp_bcf.tsv", "")
            with open(count_file, "rt") as count:
                count_reader = csv.reader(count, delimiter=" ")
                for line in count_reader:
                    counts = line[4].split(",")
                    counts = [float(i) for i in counts]
                    frequency = (counts[2] + counts[3]) / (counts[0] + counts[1] + counts[2] + counts[3])
                    # Check we have the same reference
                    assert(chrM_genome[int(line[1]) - 1] == line[2])
                    print("{}\\t{}{}{}\\t{}".format(
                                        name, chrM_genome[int(line[1]) - 2],
                                        chrM_genome[int(line[1]) - 1],
                                        chrM_genome[int(line[1])],
                                        round(frequency, 3)), file=output)
    """
}

process counts_measles {
    //publishDir "$myDir", mode: 'copy'
    memory "2G"

    input:
    set read_id, file(sam) from sammeaslesChannel

    output:
    file("*.tsv") into counts_measles

    script:
    """
    samtools view ${sam}  -O BAM  -o align.bam
    samtools sort align.bam  -o align_sorted.bam
    samtools index align_sorted.bam
    samtools idxstat align_sorted.bam > ${read_id}_measles.tsv
    """
}

process summarize {
    publishDir "$myDir", mode: 'copy'
    memory "2G"

    input:
    file(count_M) from counts_measles.toList()
    file(count_H) from counts_human.toList()

    output:
    file("count_matrix.tsv") into count_matrix

    """
    #!/usr/bin/python3
    import os
    import glob
    import csv
    count_data = {}
    for count_file in glob.glob('{0}/*.tsv'.format(os.curdir)):
        with open(count_file, "rt") as count:
            sample = os.path.basename(count_file).split(".")[0].replace("_measles","").replace("_human", "")
            count_reader = csv.reader(count, delimiter="\t")
            if sample not in count_data:
                count_data[sample] = {}
            for line in count_reader:
                if line[0] != "*":
                    count_data[sample][line[0]] = int(line[2])
    subkeys = [list(count_data[key].keys()) for key in count_data]
    subkeys_flatten = [item for subl in subkeys for item in subl]
    subkeys_unique = set(subkeys_flatten)
    with open(os.curdir + os.sep + "count_matrix.tsv", "wt") as output:
        output_writer = csv.writer(output, delimiter="\t")
        output_writer.writerow(["#Counts"] + list(count_data.keys()))
        for entite in subkeys_unique:
            output_writer.writerow([entite] + [count_data[key][entite] for key in count_data])
    """
}

process depth_mito {
    //publishDir "$myDir", mode: 'copy'
    memory "2G"

    input:
    set read_id, file(sam) from sammMitoChannel

    output:
    file("*_depth.tsv") into depth_count

    script:
    """
    samtools view ${sam}  -O BAM  -o align.bam
    samtools sort align.bam  -o align_sorted.bam
    samtools index align_sorted.bam
    samtools depth -a align_sorted.bam -o ${read_id}_depth.tsv
    """
}

process summarize_depth {
    publishDir "$myDir", mode: 'copy'
    memory "2G"

    input:
    file(depth_mito) from depth_count.toList()

    output:
    file("depth_matrix.tsv") into depth_matrix

    """
    #!/usr/bin/python3
    import os
    import glob
    import csv
    count_data = {}
    for count_file in glob.glob('{0}/*.tsv'.format(os.curdir)):
        with open(count_file, "rt") as count:
            sample = os.path.basename(count_file).split(".")[0].replace("_snp_bcf","")
            count_reader = csv.reader(count, delimiter="\t")
            if sample not in count_data:
                count_data[sample] = {}
            for line in count_reader:
                count_data[sample][int(line[1])] = int(line[2])
    subkeys = [list(count_data[key].keys()) for key in count_data]
    subkeys_flatten = [item for subl in subkeys for item in subl]
    subkeys_unique = set(subkeys_flatten)
    with open(os.curdir + os.sep + "depth_matrix.tsv", "wt") as output:
        output_writer = csv.writer(output, delimiter="\t")
        output_writer.writerow(["#Counts"] + list(count_data.keys()))
        for entite in sorted(subkeys_unique):
            output_writer.writerow([entite] + [count_data[key][entite] for key in count_data])
    """
}